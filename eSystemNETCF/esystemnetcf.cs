using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Sente.NetCL;
using Sente.NetCL.Gui;
using Sente.NetCL.Database;
using Sente.NetCL.Messages;
using Sente.NetCL.Communication;

namespace Sente.Esystemcf
{
	static class Esystemnetcf
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[MTAThread]
		static void Main()
		{
			//      string[] args = new string[1];
			//      args[0] = "emws-cf.smd";
			//      App.ReadCommandLine(args);
			//      ServerService.OnShowInformation = ShowInformation;
			//		ServerService.DefaultReadTimeOut = 20000;
			if (App.Initialize(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase))
			{
				//�adowanie parametrow z bazy esystem, jesli istnieje
				SCustomDataSet esys = DB.GetDatabase("esystem");
				bool ok = true;
				if (esys != null)
				{
					ok = LoadEsystemParameters(esys);
				}
				if (ok)
					GUI.ShowMainForm(null);
				App.TerminateApplication();
			}
		}

		private static bool LoadEsystemParameters(SCustomDataSet esys)
		{
			bool result = true;
			if (esys.OpenDatabase())
			{
				SDataView ds_operators = DB.OpenTable("", "OPERATOR", "", sFillMode.Full, "", "REF;LOGIN;DEFAULTPRINTER;GRUPA");
			  ds_operators.Filter = "LOGIN=\'" + App.User.LogOn + "\' AND AKTYWNY = 1";

				if (ds_operators.FirstRecord())
				{
					App.Values.SetValue("ACTU_OPERATOR", Sente.NetCL.ValueType.Integer, ds_operators["REF"].AsInteger);
					App.Values.SetValue("ACTU_OPERATOR_LOGIN", Sente.NetCL.ValueType.Integer, ds_operators["LOGIN"].AsString);
					App.Values.SetValue("INWENTAREF", Sente.NetCL.ValueType.Integer, 0);
					App.Values.SetValue("ACTU_OPERGROUPS", Sente.NetCL.ValueType.String, ds_operators["GRUPA"].AsString);
					App.Values.SetValue("ACTU_PRINTER", Sente.NetCL.ValueType.String, ds_operators["DEFAULTPRINTER"].AsString);
					App.Values.SetValue("ACTU_WH", Sente.NetCL.ValueType.String, "MWS");
					App.Values.SetValue("NOVCL", Sente.NetCL.ValueType.Integer, 0);
          ValuesDictionary vals = new ValuesDictionary();
          vals.SetValue("PNAME", Sente.NetCL.ValueType.String, ValueRole.InputValue, "AKTUOPERATOR");
          vals.SetValue("PVALUE", Sente.NetCL.ValueType.String, ValueRole.InputValue, ds_operators["REF"].AsString);
          DB.GetDatabase("").RunProcedure("SET_GLOBAL_PARAM", vals);
				}
				else
				{
					GUI.ShowMessage("Nie mo�na okre�li� operatora dla u�ytkownika " + App.User.LogOn, "Logowanie do aplikacji", Sente.NetCL.Gui.Enums.SMessageType.WarningOk);
					result = false;
				}
			}
			return result;
		}

		public static void ShowInformation(string msg)
		{
			GUI.ShowMessage(msg, Sente.NetCL.Gui.Enums.SMessageType.WarningOk);
		}
	}
}