namespace Sente.esystemcf.forms
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.MainMenu mainMenu1;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.sButton1 = new Sente.NetCL.Components.SButton(this.components);
      this.sTextBox1 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox2 = new Sente.NetCL.Components.STextBox(this.components);
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.menuItem1 = new System.Windows.Forms.MenuItem();
      this.menuItem2 = new System.Windows.Forms.MenuItem();
      this.menuItem3 = new System.Windows.Forms.MenuItem();
      this.label1 = new System.Windows.Forms.Label();
      this.sButton2 = new Sente.NetCL.Components.SButton(this.components);
      this.sFormPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormMenu
      // 
      this.sFormMenu.MenuItems.Add(this.menuItem1);
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.sButton2);
      this.sFormPanel.Controls.Add(this.label1);
      this.sFormPanel.Controls.Add(this.listBox1);
      this.sFormPanel.Controls.Add(this.sTextBox2);
      this.sFormPanel.Controls.Add(this.sTextBox1);
      // 
      // sButton1
      // 
      this.sButton1.Location = new System.Drawing.Point(100, 195);
      this.sButton1.Name = "sButton1";
      this.sButton1.Role = Sente.NetCL.Components.ButtonRole.None;
      this.sButton1.Size = new System.Drawing.Size(72, 20);
      this.sButton1.TabIndex = 0;
      this.sButton1.Text = "sButton1";
      // 
      // sTextBox1
      // 
      this.sTextBox1.Caption = null;
      this.sTextBox1.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox1.DataField = null;
      this.sTextBox1.DataSource = null;
      this.sTextBox1.DefinitionFile = "";
      this.sTextBox1.DictButtonVisible = false;
      this.sTextBox1.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox]";
      this.sTextBox1.Location = new System.Drawing.Point(15, 3);
      this.sTextBox1.Name = "sTextBox1";
      this.sTextBox1.SCPosition = System.Windows.Forms.DockStyle.None;
      this.sTextBox1.Size = new System.Drawing.Size(152, 22);
      this.sTextBox1.TabIndex = 0;
      this.sTextBox1.AfterEdit += new Sente.NetCL.Components.SControl.OnAfterEditEvent(this.sTextBox1_AfterEdit);
      this.sTextBox1.EventDebug += new Sente.NetCL.Components.SControl.OnEventDebug(this.sTextBox1_EventDebug);
      // 
      // sTextBox2
      // 
      this.sTextBox2.Caption = null;
      this.sTextBox2.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox2.DataField = null;
      this.sTextBox2.DataSource = null;
      this.sTextBox2.DefinitionFile = "";
      this.sTextBox2.DictButtonVisible = false;
      this.sTextBox2.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox]";
      this.sTextBox2.Location = new System.Drawing.Point(15, 31);
      this.sTextBox2.Name = "sTextBox2";
      this.sTextBox2.SCPosition = System.Windows.Forms.DockStyle.None;
      this.sTextBox2.Size = new System.Drawing.Size(152, 22);
      this.sTextBox2.TabIndex = 1;
      this.sTextBox2.EventDebug += new Sente.NetCL.Components.SControl.OnEventDebug(this.sTextBox1_EventDebug);
      // 
      // listBox1
      // 
      this.listBox1.Location = new System.Drawing.Point(17, 62);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(222, 156);
      this.listBox1.TabIndex = 2;
      // 
      // menuItem1
      // 
      this.menuItem1.MenuItems.Add(this.menuItem2);
      this.menuItem1.MenuItems.Add(this.menuItem3);
      this.menuItem1.Text = "Funkcje";
      // 
      // menuItem2
      // 
      this.menuItem2.Text = "Wyczy��";
      this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
      // 
      // menuItem3
      // 
      this.menuItem3.Text = "Blokuj";
      this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(173, 5);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(25, 20);
      this.label1.Text = "label1";
      // 
      // sButton2
      // 
      this.sButton2.Location = new System.Drawing.Point(104, 233);
      this.sButton2.Name = "sButton2";
      this.sButton2.Role = Sente.NetCL.Components.ButtonRole.None;
      this.sButton2.Size = new System.Drawing.Size(72, 20);
      this.sButton2.TabIndex = 3;
      this.sButton2.Text = "sButton2";
      this.sButton2.LostFocus += new System.EventHandler(this.sButton2_LostFocus);
      this.sButton2.Click += new System.EventHandler(this.sButton2_Click);
      this.sButton2.GotFocus += new System.EventHandler(this.sButton2_GotFocus);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "";
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Name = "Form1";
      this.Text = "Form1";
      this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
      this.sFormPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Sente.NetCL.Components.SButton sButton1;
    private Sente.NetCL.Components.STextBox sTextBox2;
    private Sente.NetCL.Components.STextBox sTextBox1;
    private System.Windows.Forms.MenuItem menuItem1;
    private System.Windows.Forms.MenuItem menuItem2;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.MenuItem menuItem3;
    private System.Windows.Forms.Label label1;
    private Sente.NetCL.Components.SButton sButton2;

  }
}