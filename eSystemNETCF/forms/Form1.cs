using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;

namespace Sente.esystemcf.forms
{
  public partial class Form1 : SForm
  {
    private int blokuj = 0;
    public Form1()
    {
      InitializeComponent();
      label1.Text = blokuj.ToString();
    }

    protected override void SetBaseComponents()
    {
      base._sFormComponents = this.components;
    }

    protected override void SetComponentsFromBase(IContainer container)
    {
      components = container;
    }

    private void sTextBox1_EventDebug(object sender, string debugText)
    {
      listBox1.Items.Add(debugText);
    }

    private void menuItem2_Click(object sender, EventArgs e)
    {
      listBox1.Items.Clear();
    }

    private void Form1_Closing(object sender, CancelEventArgs e)
    {
      listBox1.Items.Add("SFOrm: Closing :"+e.Cancel.ToString());
     // e.Cancel = true;
    }

    private void menuItem3_Click(object sender, EventArgs e)
    {
      blokuj++;
      if (blokuj == 3) blokuj = 0;
      label1.Text = blokuj.ToString();
    }

    private void sTextBox1_AfterEdit(object sender, CancelEventArgs e)
    {
      if (blokuj == 2)
        GUI.ShowMessage("Blad", "Blad", Sente.NetCL.Gui.Enums.SMessageType.Error);
      if (blokuj > 0)
        e.Cancel = true;
    }

    private void sButton2_Click(object sender, EventArgs e)
    {
      listBox1.Items.Add("Button: Click");
      Close();
    }

    private void sButton2_GotFocus(object sender, EventArgs e)
    {
      listBox1.Items.Add("Button: GotFocus");
    }

    private void sButton2_LostFocus(object sender, EventArgs e)
    {
      listBox1.Items.Add("Button: LostFocus");
    }
  }
}