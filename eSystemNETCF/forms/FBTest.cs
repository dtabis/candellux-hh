﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL.Components;

namespace Sente.esystemcf
{
  public partial class FBTest :SForm
  {
    public FBTest():base()
    {
      InitializeComponent();      
    }

    protected override void SetBaseComponents()
    {
      base.components = components;
    }

    protected override void SetComponentsFromBase(IContainer container)
    {
      components = container;
    }
  }
}