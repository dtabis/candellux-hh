﻿namespace Sente.esystemcf
{
  partial class FBTest
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;
   // private System.Windows.Forms.MainMenu mainMenu1;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose();
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.KlienciBindingSource = new Sente.NetCL.Database.SBindingSource(this.components);
      this.sLabel1 = new Sente.NetCL.Components.SLabel(this.components);
      this.sTextBox1 = new Sente.NetCL.Components.STextBox(this.components);
      this.SuspendLayout();
      // 
      // KlienciBindingSource
      // 
      this.KlienciBindingSource.Active = false;
      this.KlienciBindingSource.AutoActiveEnabled = true;
      this.KlienciBindingSource.Database = "esystem";
      this.KlienciBindingSource.ExpectedFields = "FSKROT;NAZWA;MIASTO";
      this.KlienciBindingSource.Initialized = true;
      this.KlienciBindingSource.LocalDataTable = true;
      this.KlienciBindingSource.QuerySQL = "";
      this.KlienciBindingSource.TableName = "KLIENCI";
      // 
      // sLabel1
      // 
      //this.sLabel1.DataField = "";
      //this.sLabel1.DataSource = this.KlienciBindingSource;
      //this.sLabel1.DefinitionFile = "";
      this.sLabel1.Location = new System.Drawing.Point(3, 23);
      this.sLabel1.Name = "sLabel1";
      this.sLabel1.Size = new System.Drawing.Size(82, 18);
      this.sLabel1.TabIndex = 0;
      // 
      // sTextBox1
      // 
      this.sTextBox1.DataField = "MIASTO";
      this.sTextBox1.DataSource = this.KlienciBindingSource;
      this.sTextBox1.DefinitionFile = "";
      this.sTextBox1.Location = new System.Drawing.Point(80, 23);
      this.sTextBox1.Name = "sTextBox1";
      this.sTextBox1.Size = new System.Drawing.Size(140, 20);
      this.sTextBox1.TabIndex = 1;
      // 
      // FBTest
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Controls.Add(this.sTextBox1);
      this.Controls.Add(this.sLabel1);
      this.Name = "FBTest";
      this.Text = "FBTest";
      this.ResumeLayout(false);

    }

    #endregion

    private Sente.NetCL.Components.SLabel sLabel1;
    private Sente.NetCL.Components.STextBox sTextBox1;
    public Sente.NetCL.Database.SBindingSource KlienciBindingSource;
  }
}