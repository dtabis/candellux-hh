using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL.Gui.Enums;
using Sente.NetCL;
using Sente.NetCL.Database;

namespace Sente.esystemcf.formsProd
{
  public partial class EditRealPRSCHEDOEPRForm : SForm
  {
    private string _showtype;
    private string _prschedoper;
    #region Constructor and override methods
    public EditRealPRSCHEDOEPRForm()
    {
      InitializeComponent();
    }
        protected override void SetBaseComponents()
    {
      base._sFormComponents = components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      this.components = container;
    }
    #endregion

    private bool EditRealPRSCHEDOEPRForm_FormShow(object sender)
    {
      string sprschedoper = app.Values["PRSCHEDOPER"].AsString;
      app.Values.SetValue("PRNAGZAM", Sente.NetCL.ValueType.Integer, 0);
      _showtype = app.Values["PRSCHEDOPERSHOWTYPE"].AsString;
      if (string.IsNullOrEmpty(_showtype))
        _showtype = "0";
      app.Values.SetValue("PRSCHEDOPERSHOWTYPE", Sente.NetCL.ValueType.Unknown, "");
      try
      {
        _prschedoper = int.Parse(sprschedoper).ToString();
      }
      catch (Exception e)
      {
        GUI.ShowMessage("Brak wskazania operacji produkcyjnej", "Wyb�r operacji produkcyjnej", SMessageType.Stop);
        return false;
      }
      DataPRSCHEDOPERS.DataView.FindRecord("REF", _prschedoper, SFindModeParam.Exact, SFindBeginParam.FromBegin, SFindRefreshParam.None);
      DataPROPERSRAPS.DataView.Filter = "[PRSCHEDOPER]=" + _prschedoper.ToString();
      DataPRSHORTAGES.DataView.Filter = "[PRSCHEDOPER]=" + _prschedoper.ToString();
      //if(PRSCHEDOPERS_STATUS == 0) SSEdit1->Text = "Nierozpocz�ta";
      //else if(PRSCHEDOPERS_STATUS == 1) SSEdit1->Text = "Gotowa do realizacji";
      //else if(PRSCHEDOPERS_STATUS == 2) SSEdit1->Text = "W trakcie realizacji";
      //else SSEdit1->Text = "Zrealizowana";
      if (_showtype == "1")
        this.AddButton.PerformClick();
      return true;
    }

    private void AddButton_Click(object sender, EventArgs e)
    {
      DialogResult res = DialogResult.None;
      if (MainTabControl.SelectedIndex == 0)
      {
        res = GUI.NewRecord(DataPROPERSRAPS.DataView.TableName, "EditPROPERSRAPS", "Edycja raportu");
      }
      else
      {
        res = GUI.NewRecord(DataPRSHORTAGES.DataView.TableName, "EditPRSHORTAGES", "Raport brak�w");
      }
      if(res == DialogResult.OK)
        DataPRSCHEDOPERS.DataView.Refresh(SRefreshMode.CurrentRow);
      if (MainTabControl.SelectedIndex == 0)
      {
        SDataView _dv = DataPRSCHEDOPERS.DataView;
        if (_dv["AMOUNTRESULT"].AsNumeric != (_dv["AMOUNTIN"].AsNumeric + _dv["AMOUNTSHORTAGES"].AsNumeric))
        {
          //jeszcze raport nie zakocznoiny - wiec podpowiadam dalsze akcje
          if (res == DialogResult.OK)
          {
            //dodanie kolejnego raportu
            AddButton.PerformClick();
          }
          else
          {
            //dodanie braku
            MainTabControl.SelectedIndex = 1;
            AddButton.PerformClick();
          }
        }
      }
    }

    private void EditButton_Click(object sender, EventArgs e)
    {
      DialogResult res = DialogResult.None;
      if (MainTabControl.SelectedIndex == 0)
      {
        res = GUI.EditRecord(DataPROPERSRAPS.DataView.TableName, "EditPROPERSRAPS", "Modyfikowanie raportu");
      }
      else
      {
        res = GUI.EditRecord(DataPRSHORTAGES.DataView.TableName, "EditPRSHORTAGES", "Modyfikowanie braku");
      }
      if (res == DialogResult.OK)
        DataPRSCHEDOPERS.DataView.Refresh(SRefreshMode.CurrentRow);
    }

    private void DelButton_Click(object sender, EventArgs e)
    {
      DialogResult res = DialogResult.None;
      if (MainTabControl.SelectedIndex == 0)
      {
        res = GUI.DeleteRecord(DataPROPERSRAPS.DataView.TableName);
      }
      else
      {
        res = GUI.DeleteRecord(DataPRSHORTAGES.DataView.TableName);
      }
      if (res == DialogResult.OK)
        DataPRSCHEDOPERS.DataView.Refresh(SRefreshMode.CurrentRow);
    }

    private void ApplyButton_Click(object sender, EventArgs e)
    {
      if (DB.GetDatabase(null).RunSql("update prschedopers set status = 3 where ref = 0" + _prschedoper.ToString()))
      {
        GUI.ShowMessage("Znacznik realizacji operacji ustawiony", "Realizacja operacji produkcyjnej", SMessageType.Information);
        DialogResult = DialogResult.OK;
      }
      else
      {
        GUI.ShowMessage("Niezachowany bilans surowcowy. \n Ustawienie znacznika realizacji operacji nieudane", "Realizacja operacji produkcyjnej", SMessageType.Stop);
      }

    }
    

  }
}