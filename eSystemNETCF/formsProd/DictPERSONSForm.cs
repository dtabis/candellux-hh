using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL;


namespace Sente.esystemcf.formsProd
{
  public partial class DictPERSONSForm : SForm
  {
    public DictPERSONSForm()
    {
      InitializeComponent();
    }
    protected override void SetBaseComponents()
    {
      base._sFormComponents = this.components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      this.components = container;
    }
  }
}