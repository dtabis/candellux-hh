using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL.Components;
using Sente.ESystem.FormsProd;

namespace Sente.esystemcf.formsProd
{
  public partial class ProdMainForm : SForm
  {
    public ProdMainForm()
    {
      InitializeComponent();
    }

    protected override void SetBaseComponents()
    {
      base._sFormComponents = this.components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      this.components = container;
    }

    private void sButton2_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void sButton1_Click(object sender, EventArgs e)
    {
      new ProdFunc().PrSchedopersReal();
    }

    private void sButton3_Click(object sender, EventArgs e)
    {
      GUI.DisplayWindow("FormTest", "", SFormMode.Dialog, true, "", "", false);
    }


   
  }
}