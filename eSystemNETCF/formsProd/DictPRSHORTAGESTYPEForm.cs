using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL.Gui.Enums;


namespace Sente.esystemcf.formsProd
{
  public partial class DictPRSHORTAGESTYPEForm : SForm
  {
    #region Constructor and override methods
    public DictPRSHORTAGESTYPEForm()
    {
      InitializeComponent();
    }
    protected override void SetBaseComponents()
    {
      base._sFormComponents = components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      components = container;
    }
    #endregion

  }
}