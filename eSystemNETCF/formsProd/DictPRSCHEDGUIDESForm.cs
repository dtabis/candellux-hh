using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL.Gui.Enums;

namespace Sente.esystemcf.formsProd
{
  public partial class DictPRSCHEDGUIDESForm : SForm
  {
    #region Constructor and override methods
    public DictPRSCHEDGUIDESForm()
    {
      InitializeComponent();
    }
    protected override void SetBaseComponents()
    {
      base._sFormComponents = components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      components = container;
    }
    #endregion

    private void sButton1_Click(object sender, EventArgs e)
    {
      if (DataPRSCHEDGUIDES.DataView.Count > 0)
        this.DialogResult = DialogResult.OK;
      else
        GUI.ShowMessage("Brak okre�lonych przewodnik�w", "Wyb�r przewodnika", SMessageType.Stop);
    }

    private void sButton2_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

  }
}