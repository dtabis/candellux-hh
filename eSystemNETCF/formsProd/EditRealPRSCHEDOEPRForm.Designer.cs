namespace Sente.esystemcf.formsProd
{
  partial class EditRealPRSCHEDOEPRForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRealPRSCHEDOEPRForm));
      this.TopPanel = new Sente.NetCL.Components.SPanel();
      this.sTextBox5 = new Sente.NetCL.Components.STextBox(this.components);
      this.DataPRSCHEDOPERS = new Sente.NetCL.Database.SBindingSource(this.components);
      this.sTextBox4 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox3 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox2 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox1 = new Sente.NetCL.Components.STextBox(this.components);
      this.ButtonPanel = new Sente.NetCL.Components.SPanel();
      this.ApplyButton = new Sente.NetCL.Components.SButton(this.components);
      this.CancelButton = new Sente.NetCL.Components.SButton(this.components);
      this.DelButton = new Sente.NetCL.Components.SButton(this.components);
      this.EditButton = new Sente.NetCL.Components.SButton(this.components);
      this.AddButton = new Sente.NetCL.Components.SButton(this.components);
      this.MainTabControl = new Sente.NetCL.Components.STabControl(this.components);
      this.RaportyPage = new System.Windows.Forms.TabPage();
      this.PRSHEDRAPSDataGrid = new Sente.NetCL.Components.SDataGrid(this.components);
      this.DataPROPERSRAPS = new Sente.NetCL.Database.SBindingSource(this.components);
      this.BrakiPage = new System.Windows.Forms.TabPage();
      this.PRSHORTAGESDataGrid = new Sente.NetCL.Components.SDataGrid(this.components);
      this.DataPRSHORTAGES = new Sente.NetCL.Database.SBindingSource(this.components);
      this.sFormPanel.SuspendLayout();
      this.TopPanel.SuspendLayout();
      this.ButtonPanel.SuspendLayout();
      this.MainTabControl.SuspendLayout();
      this.RaportyPage.SuspendLayout();
      this.BrakiPage.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.MainTabControl);
      this.sFormPanel.Controls.Add(this.ButtonPanel);
      this.sFormPanel.Controls.Add(this.TopPanel);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 270);
      // 
      // TopPanel
      // 
      this.TopPanel.Controls.Add(this.sTextBox5);
      this.TopPanel.Controls.Add(this.sTextBox4);
      this.TopPanel.Controls.Add(this.sTextBox3);
      this.TopPanel.Controls.Add(this.sTextBox2);
      this.TopPanel.Controls.Add(this.sTextBox1);
      this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
      this.TopPanel.Location = new System.Drawing.Point(0, 0);
      this.TopPanel.Name = "TopPanel";
      this.TopPanel.Size = new System.Drawing.Size(240, 69);
      // 
      // sTextBox5
      // 
      this.sTextBox5.BackColor = System.Drawing.SystemColors.Window;
      this.sTextBox5.BindingValue = null;
      this.sTextBox5.Caption = "Wyj";
      this.sTextBox5.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox5.DataField = "AMOUNTRESULT";
      this.sTextBox5.DataSource = this.DataPRSCHEDOPERS;
      this.sTextBox5.DefinitionFile = "";
      this.sTextBox5.DictButtonText = "F3";
      this.sTextBox5.DictButtonVisible = false;
      this.sTextBox5.Enabled = false;
      this.sTextBox5.Id = resources.GetString("sTextBox5.Id");
      this.sTextBox5.Location = new System.Drawing.Point(160, 45);
      this.sTextBox5.Name = "sTextBox5";
      this.sTextBox5.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox5.Size = new System.Drawing.Size(77, 21);
      this.sTextBox5.TabIndex = 3;
      // 
      // DataPRSCHEDOPERS
      // 
      this.DataPRSCHEDOPERS.Active = false;
      this.DataPRSCHEDOPERS.AutoActiveEnabled = true;
      this.DataPRSCHEDOPERS.Database = "";
      this.DataPRSCHEDOPERS.ExpectedFields = "";
      this.DataPRSCHEDOPERS.Id = "PRSCHEDOPERS - [Sente.NetCL.Database.SBindingSource]";
      this.DataPRSCHEDOPERS.Initialized = false;
      this.DataPRSCHEDOPERS.LocalDataTable = false;
      this.DataPRSCHEDOPERS.QuerySql = "";
      this.DataPRSCHEDOPERS.TableName = "PRSCHEDOPERS";
      // 
      // sTextBox4
      // 
      this.sTextBox4.BackColor = System.Drawing.SystemColors.Window;
      this.sTextBox4.BindingValue = null;
      this.sTextBox4.Caption = "Br";
      this.sTextBox4.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox4.DataField = "AMOUNTSHORTAGES";
      this.sTextBox4.DataSource = this.DataPRSCHEDOPERS;
      this.sTextBox4.DefinitionFile = "";
      this.sTextBox4.DictButtonText = "F3";
      this.sTextBox4.DictButtonVisible = false;
      this.sTextBox4.Enabled = false;
      this.sTextBox4.Id = resources.GetString("sTextBox4.Id");
      this.sTextBox4.Location = new System.Drawing.Point(92, 45);
      this.sTextBox4.Name = "sTextBox4";
      this.sTextBox4.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox4.Size = new System.Drawing.Size(62, 21);
      this.sTextBox4.TabIndex = 2;
      // 
      // sTextBox3
      // 
      this.sTextBox3.BackColor = System.Drawing.SystemColors.Window;
      this.sTextBox3.BindingValue = null;
      this.sTextBox3.Caption = "Wej";
      this.sTextBox3.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox3.DataField = "AMOUNTIN";
      this.sTextBox3.DataSource = this.DataPRSCHEDOPERS;
      this.sTextBox3.DefinitionFile = "";
      this.sTextBox3.DictButtonText = "F3";
      this.sTextBox3.DictButtonVisible = false;
      this.sTextBox3.Enabled = false;
      this.sTextBox3.Id = resources.GetString("sTextBox3.Id");
      this.sTextBox3.Location = new System.Drawing.Point(3, 45);
      this.sTextBox3.Name = "sTextBox3";
      this.sTextBox3.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox3.Size = new System.Drawing.Size(77, 21);
      this.sTextBox3.TabIndex = 1;
      // 
      // sTextBox2
      // 
      this.sTextBox2.BackColor = System.Drawing.SystemColors.Window;
      this.sTextBox2.BindingValue = null;
      this.sTextBox2.Caption = "Przew.";
      this.sTextBox2.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox2.DataField = "ZAMOWIENIE^ID";
      this.sTextBox2.DataSource = this.DataPRSCHEDOPERS;
      this.sTextBox2.DefinitionFile = "";
      this.sTextBox2.DictButtonText = "F3";
      this.sTextBox2.DictButtonVisible = false;
      this.sTextBox2.Enabled = false;
      this.sTextBox2.Id = resources.GetString("sTextBox2.Id");
      this.sTextBox2.Location = new System.Drawing.Point(3, 22);
      this.sTextBox2.Name = "sTextBox2";
      this.sTextBox2.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox2.Size = new System.Drawing.Size(234, 21);
      this.sTextBox2.TabIndex = 1;
      // 
      // sTextBox1
      // 
      this.sTextBox1.BackColor = System.Drawing.SystemColors.Window;
      this.sTextBox1.BindingValue = null;
      this.sTextBox1.Caption = "Operacja";
      this.sTextBox1.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox1.DataField = "SHOPER^DESCRIPT";
      this.sTextBox1.DataSource = this.DataPRSCHEDOPERS;
      this.sTextBox1.DefinitionFile = "";
      this.sTextBox1.DictButtonText = "F3";
      this.sTextBox1.DictButtonVisible = false;
      this.sTextBox1.Enabled = false;
      this.sTextBox1.Id = resources.GetString("sTextBox1.Id");
      this.sTextBox1.Location = new System.Drawing.Point(3, 0);
      this.sTextBox1.Name = "sTextBox1";
      this.sTextBox1.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox1.Size = new System.Drawing.Size(234, 21);
      this.sTextBox1.TabIndex = 0;
      // 
      // ButtonPanel
      // 
      this.ButtonPanel.Controls.Add(this.ApplyButton);
      this.ButtonPanel.Controls.Add(this.CancelButton);
      this.ButtonPanel.Controls.Add(this.DelButton);
      this.ButtonPanel.Controls.Add(this.EditButton);
      this.ButtonPanel.Controls.Add(this.AddButton);
      this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.ButtonPanel.Location = new System.Drawing.Point(0, 221);
      this.ButtonPanel.Name = "ButtonPanel";
      this.ButtonPanel.Size = new System.Drawing.Size(240, 49);
      // 
      // ApplyButton
      // 
      this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.ApplyButton.Location = new System.Drawing.Point(87, 26);
      this.ApplyButton.Name = "ApplyButton";
      this.ApplyButton.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.ApplyButton.Size = new System.Drawing.Size(72, 20);
      this.ApplyButton.TabIndex = 4;
      this.ApplyButton.Text = "Dalej";
      this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
      // 
      // CancelButton
      // 
      this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CancelButton.Location = new System.Drawing.Point(165, 26);
      this.CancelButton.Name = "CancelButton";
      this.CancelButton.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.CancelButton.Size = new System.Drawing.Size(72, 20);
      this.CancelButton.TabIndex = 3;
      this.CancelButton.Text = "Anuluj";
      // 
      // DelButton
      // 
      this.DelButton.Location = new System.Drawing.Point(159, 3);
      this.DelButton.Name = "DelButton";
      this.DelButton.Role = Sente.NetCL.Components.ButtonRole.None;
      this.DelButton.Size = new System.Drawing.Size(72, 20);
      this.DelButton.TabIndex = 2;
      this.DelButton.Text = "Usu�";
      this.DelButton.Click += new System.EventHandler(this.DelButton_Click);
      // 
      // EditButton
      // 
      this.EditButton.Location = new System.Drawing.Point(81, 3);
      this.EditButton.Name = "EditButton";
      this.EditButton.Role = Sente.NetCL.Components.ButtonRole.None;
      this.EditButton.Size = new System.Drawing.Size(72, 20);
      this.EditButton.TabIndex = 1;
      this.EditButton.Text = "Popraw";
      this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
      // 
      // AddButton
      // 
      this.AddButton.Location = new System.Drawing.Point(3, 3);
      this.AddButton.Name = "AddButton";
      this.AddButton.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.AddButton.Size = new System.Drawing.Size(72, 20);
      this.AddButton.TabIndex = 0;
      this.AddButton.Text = "Dodaj";
      this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
      // 
      // MainTabControl
      // 
      this.MainTabControl.Controls.Add(this.RaportyPage);
      this.MainTabControl.Controls.Add(this.BrakiPage);
      this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.MainTabControl.Location = new System.Drawing.Point(0, 69);
      this.MainTabControl.Name = "MainTabControl";
      this.MainTabControl.SelectedIndex = 0;
      this.MainTabControl.Size = new System.Drawing.Size(240, 152);
      this.MainTabControl.TabIndex = 2;
      this.MainTabControl.TabStop = false;
      // 
      // RaportyPage
      // 
      this.RaportyPage.Controls.Add(this.PRSHEDRAPSDataGrid);
      this.RaportyPage.Location = new System.Drawing.Point(0, 0);
      this.RaportyPage.Name = "RaportyPage";
      this.RaportyPage.Size = new System.Drawing.Size(240, 129);
      this.RaportyPage.Text = "Raporty";
      // 
      // PRSHEDRAPSDataGrid
      // 
      this.PRSHEDRAPSDataGrid.BindingValue = null;
      this.PRSHEDRAPSDataGrid.Caption = null;
      this.PRSHEDRAPSDataGrid.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.PRSHEDRAPSDataGrid.DataField = null;
      this.PRSHEDRAPSDataGrid.DataSource = this.DataPROPERSRAPS;
      this.PRSHEDRAPSDataGrid.DefinitionFile = "";
      this.PRSHEDRAPSDataGrid.DictButtonText = "";
      this.PRSHEDRAPSDataGrid.DictButtonVisible = false;
      this.PRSHEDRAPSDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PRSHEDRAPSDataGrid.Id = resources.GetString("PRSHEDRAPSDataGrid.Id");
      this.PRSHEDRAPSDataGrid.Location = new System.Drawing.Point(0, 0);
      this.PRSHEDRAPSDataGrid.ModifiedCols = null;
      this.PRSHEDRAPSDataGrid.Name = "PRSHEDRAPSDataGrid";
      this.PRSHEDRAPSDataGrid.SCPosition = System.Windows.Forms.DockStyle.None;
      this.PRSHEDRAPSDataGrid.SearchRapid = true;
      this.PRSHEDRAPSDataGrid.SearchTextBox = null;
      this.PRSHEDRAPSDataGrid.Size = new System.Drawing.Size(240, 129);
      this.PRSHEDRAPSDataGrid.TabIndex = 0;
      // 
      // DataPROPERSRAPS
      // 
      this.DataPROPERSRAPS.Active = false;
      this.DataPROPERSRAPS.AutoActiveEnabled = true;
      this.DataPROPERSRAPS.Database = "";
      this.DataPROPERSRAPS.ExpectedFields = "MAKETIME;EMPLOYEE^PERSONNAMES;AMOUNT;SETUPTIME;PRSCHEDOPER;EMPLOYEE^FILENO;ECONTR" +
          "ACTSDEF^NAME;ECONTRACTSPOSDEF";
      this.DataPROPERSRAPS.Id = "PROPERSRAPS - [Sente.NetCL.Database.SBindingSource]";
      this.DataPROPERSRAPS.Initialized = false;
      this.DataPROPERSRAPS.LocalDataTable = true;
      this.DataPROPERSRAPS.QuerySql = "";
      this.DataPROPERSRAPS.TableName = "PROPERSRAPS";
      // 
      // BrakiPage
      // 
      this.BrakiPage.Controls.Add(this.PRSHORTAGESDataGrid);
      this.BrakiPage.Location = new System.Drawing.Point(0, 0);
      this.BrakiPage.Name = "BrakiPage";
      this.BrakiPage.Size = new System.Drawing.Size(232, 126);
      this.BrakiPage.Text = "Braki";
      // 
      // PRSHORTAGESDataGrid
      // 
      this.PRSHORTAGESDataGrid.BindingValue = null;
      this.PRSHORTAGESDataGrid.Caption = null;
      this.PRSHORTAGESDataGrid.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.PRSHORTAGESDataGrid.DataField = null;
      this.PRSHORTAGESDataGrid.DataSource = this.DataPRSHORTAGES;
      this.PRSHORTAGESDataGrid.DefinitionFile = "";
      this.PRSHORTAGESDataGrid.DictButtonText = "";
      this.PRSHORTAGESDataGrid.DictButtonVisible = false;
      this.PRSHORTAGESDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PRSHORTAGESDataGrid.Id = resources.GetString("PRSHORTAGESDataGrid.Id");
      this.PRSHORTAGESDataGrid.Location = new System.Drawing.Point(0, 0);
      this.PRSHORTAGESDataGrid.ModifiedCols = null;
      this.PRSHORTAGESDataGrid.Name = "PRSHORTAGESDataGrid";
      this.PRSHORTAGESDataGrid.SCPosition = System.Windows.Forms.DockStyle.None;
      this.PRSHORTAGESDataGrid.SearchRapid = true;
      this.PRSHORTAGESDataGrid.SearchTextBox = null;
      this.PRSHORTAGESDataGrid.Size = new System.Drawing.Size(232, 126);
      this.PRSHORTAGESDataGrid.TabIndex = 0;
      // 
      // DataPRSHORTAGES
      // 
      this.DataPRSHORTAGES.Active = false;
      this.DataPRSHORTAGES.AutoActiveEnabled = true;
      this.DataPRSHORTAGES.Database = "";
      this.DataPRSHORTAGES.ExpectedFields = "KTM;KTM^NAZWA;PRSHORTAGESTYPE^DESCRIPT;AMOUNT;PERSON^PERSON;PRSCHEDOPER;PRSHORTAG" +
          "ESTYPE;PRSCHEDOPERDECL^NUMBER;PRDEPART;PRSCHEDGUIDESPOS;PRPOZZAM";
      this.DataPRSHORTAGES.Id = "PRSHORTAGES - [Sente.NetCL.Database.SBindingSource]";
      this.DataPRSHORTAGES.Initialized = false;
      this.DataPRSHORTAGES.LocalDataTable = true;
      this.DataPRSHORTAGES.QuerySql = "";
      this.DataPRSHORTAGES.TableName = "PRSHORTAGES";
      // 
      // EditRealPRSCHEDOEPRForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "Raport operacji";
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Name = "EditRealPRSCHEDOEPRForm";
      this.FormShow += new Sente.NetCL.Gui.SForm.OnFormShowEventHandler(this.EditRealPRSCHEDOEPRForm_FormShow);
      this.sFormPanel.ResumeLayout(false);
      this.TopPanel.ResumeLayout(false);
      this.ButtonPanel.ResumeLayout(false);
      this.MainTabControl.ResumeLayout(false);
      this.RaportyPage.ResumeLayout(false);
      this.BrakiPage.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Sente.NetCL.Components.SPanel TopPanel;
    private Sente.NetCL.Components.STextBox sTextBox2;
    private Sente.NetCL.Components.STextBox sTextBox1;
    private Sente.NetCL.Components.STextBox sTextBox3;
    private Sente.NetCL.Components.STextBox sTextBox5;
    private Sente.NetCL.Components.STextBox sTextBox4;
    private Sente.NetCL.Components.SPanel ButtonPanel;
    private Sente.NetCL.Components.SButton ApplyButton;
    private Sente.NetCL.Components.SButton CancelButton;
    private Sente.NetCL.Components.SButton DelButton;
    private Sente.NetCL.Components.SButton EditButton;
    private Sente.NetCL.Components.SButton AddButton;
    private Sente.NetCL.Components.STabControl MainTabControl;
    private System.Windows.Forms.TabPage RaportyPage;
    private System.Windows.Forms.TabPage BrakiPage;
    private Sente.NetCL.Database.SBindingSource DataPRSCHEDOPERS;
    private Sente.NetCL.Database.SBindingSource DataPROPERSRAPS;
    private Sente.NetCL.Database.SBindingSource DataPRSHORTAGES;
    private Sente.NetCL.Components.SDataGrid PRSHORTAGESDataGrid;
    private Sente.NetCL.Components.SDataGrid PRSHEDRAPSDataGrid;
  }
}