namespace Sente.esystemcf.formsProd
{
  partial class DictPERSONSForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.DataPERSONS = new Sente.NetCL.Database.SBindingSource(this.components);
      this.sPanel1 = new Sente.NetCL.Components.SPanel();
      this.sButton2 = new Sente.NetCL.Components.SButton(this.components);
      this.sButton1 = new Sente.NetCL.Components.SButton(this.components);
      this.SearchTextBox = new Sente.NetCL.Components.STextBox(this.components);
      this.PERSONSGrid = new Sente.NetCL.Components.SDataGrid(this.components);
      this.sFormPanel.SuspendLayout();
      this.sPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.PERSONSGrid);
      this.sFormPanel.Controls.Add(this.SearchTextBox);
      this.sFormPanel.Controls.Add(this.sPanel1);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 270);
      // 
      // DataPERSONS
      // 
      this.DataPERSONS.Active = false;
      this.DataPERSONS.AutoActiveEnabled = true;
      this.DataPERSONS.Database = "esystem";
      this.DataPERSONS.ExpectedFields = "PERSON;PESEL;NIP";
      this.DataPERSONS.Id = "PERSONS - [Sente.NetCL.Database.SBindingSource]";
      this.DataPERSONS.Initialized = false;
      this.DataPERSONS.LocalDataTable = false;
      this.DataPERSONS.QuerySql = "";
      this.DataPERSONS.TableName = "PERSONS";
      // 
      // sPanel1
      // 
      this.sPanel1.Controls.Add(this.sButton2);
      this.sPanel1.Controls.Add(this.sButton1);
      this.sPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.sPanel1.Location = new System.Drawing.Point(0, 240);
      this.sPanel1.Name = "sPanel1";
      this.sPanel1.Size = new System.Drawing.Size(240, 30);
      // 
      // sButton2
      // 
      this.sButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.sButton2.Location = new System.Drawing.Point(159, 2);
      this.sButton2.Name = "sButton2";
      this.sButton2.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.sButton2.Size = new System.Drawing.Size(75, 25);
      this.sButton2.TabIndex = 3;
      this.sButton2.Text = "Anuluj";
      // 
      // sButton1
      // 
      this.sButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.sButton1.Location = new System.Drawing.Point(78, 2);
      this.sButton1.Name = "sButton1";
      this.sButton1.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.sButton1.Size = new System.Drawing.Size(75, 25);
      this.sButton1.TabIndex = 2;
      this.sButton1.Text = "Zatwierd�";
      // 
      // SearchTextBox
      // 
      this.SearchTextBox.BindingValue = null;
      this.SearchTextBox.Caption = null;
      this.SearchTextBox.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Developer;
      this.SearchTextBox.DataField = null;
      this.SearchTextBox.DataSource = null;
      this.SearchTextBox.DefinitionFile = "";
      this.SearchTextBox.DictButtonText = "S";
      this.SearchTextBox.DictButtonVisible = true;
      this.SearchTextBox.Dock = System.Windows.Forms.DockStyle.Top;
      this.SearchTextBox.Id = " - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox]";
      this.SearchTextBox.Location = new System.Drawing.Point(0, 0);
      this.SearchTextBox.Name = "SearchTextBox";
      this.SearchTextBox.SCPosition = System.Windows.Forms.DockStyle.None;
      this.SearchTextBox.Size = new System.Drawing.Size(240, 21);
      this.SearchTextBox.TabIndex = 1;
      // 
      // PERSONSGrid
      // 
      this.PERSONSGrid.BindingValue = null;
      this.PERSONSGrid.Caption = null;
      this.PERSONSGrid.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.PERSONSGrid.DataField = null;
      this.PERSONSGrid.DataSource = this.DataPERSONS;
      this.PERSONSGrid.DefinitionFile = "";
      this.PERSONSGrid.DictButtonText = "";
      this.PERSONSGrid.DictButtonVisible = false;
      this.PERSONSGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PERSONSGrid.Id = "SDataGrid - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGri" +
          "d] - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGrid] - [" +
          "Sente.NetCL.Components.SDataGrid]";
      this.PERSONSGrid.Location = new System.Drawing.Point(0, 21);
      this.PERSONSGrid.ModifiedCols = null;
      this.PERSONSGrid.Name = "PERSONSGrid";
      this.PERSONSGrid.SCPosition = System.Windows.Forms.DockStyle.None;
      this.PERSONSGrid.SearchRapid = false;
      this.PERSONSGrid.SearchTextBox = this.SearchTextBox;
      this.PERSONSGrid.Size = new System.Drawing.Size(240, 219);
      this.PERSONSGrid.TabIndex = 0;
      // 
      // DictPERSONSForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "Wybierz pracownika do raportu braku";
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Name = "DictPERSONSForm";
      this.Text = "DictPERSONS";
      this.sFormPanel.ResumeLayout(false);
      this.sPanel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion
    private Sente.NetCL.Database.SBindingSource DataPERSONS;
    private Sente.NetCL.Components.STextBox SearchTextBox;
    private Sente.NetCL.Components.SPanel sPanel1;
    private Sente.NetCL.Components.SButton sButton2;
    private Sente.NetCL.Components.SButton sButton1;
    private Sente.NetCL.Components.SDataGrid PERSONSGrid;
  }
}