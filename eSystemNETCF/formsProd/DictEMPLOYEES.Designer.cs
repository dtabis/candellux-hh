namespace Sente.esystemcf.formsProd
{
  partial class DictEMPLOYEES
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.DataEMPLOYEES = new Sente.NetCL.Database.SBindingSource(this.components);
      this.sPanel1 = new Sente.NetCL.Components.SPanel();
      this.sButton2 = new Sente.NetCL.Components.SButton(this.components);
      this.sButton1 = new Sente.NetCL.Components.SButton(this.components);
      this.SearchTextBox = new Sente.NetCL.Components.STextBox(this.components);
      this.EMPLOYEESGrid = new Sente.NetCL.Components.SDataGrid(this.components);
      this.sFormPanel.SuspendLayout();
      this.sPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.EMPLOYEESGrid);
      this.sFormPanel.Controls.Add(this.SearchTextBox);
      this.sFormPanel.Controls.Add(this.sPanel1);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 270);
      // 
      // DataECONTRACTSDEF
      // 
      this.DataEMPLOYEES.Active = false;
      this.DataEMPLOYEES.AutoActiveEnabled = true;
      this.DataEMPLOYEES.Database = "esystem";
      this.DataEMPLOYEES.ExpectedFields = "FILENO;PERSONNAMES;DEPARTMENT";
      this.DataEMPLOYEES.Id = "ECONTRACTSDEF - [Sente.NetCL.Database.SBindingSource]";
      this.DataEMPLOYEES.Initialized = false;
      this.DataEMPLOYEES.LocalDataTable = false;
      this.DataEMPLOYEES.QuerySql = "";
      this.DataEMPLOYEES.TableName = "EMPLOYEES";
      // 
      // sPanel1
      // 
      this.sPanel1.Controls.Add(this.sButton2);
      this.sPanel1.Controls.Add(this.sButton1);
      this.sPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.sPanel1.Location = new System.Drawing.Point(0, 240);
      this.sPanel1.Name = "sPanel1";
      this.sPanel1.Size = new System.Drawing.Size(240, 30);
      // 
      // sButton2
      // 
      this.sButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.sButton2.Location = new System.Drawing.Point(159, 2);
      this.sButton2.Name = "sButton2";
      this.sButton2.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.sButton2.Size = new System.Drawing.Size(75, 25);
      this.sButton2.TabIndex = 3;
      this.sButton2.Text = "Anuluj";
      // 
      // sButton1
      // 
      this.sButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.sButton1.Location = new System.Drawing.Point(78, 2);
      this.sButton1.Name = "sButton1";
      this.sButton1.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.sButton1.Size = new System.Drawing.Size(75, 25);
      this.sButton1.TabIndex = 2;
      this.sButton1.Text = "Zatwierd�";
      // 
      // SearchTextBox
      // 
      this.SearchTextBox.BindingValue = null;
      this.SearchTextBox.Caption = null;
      this.SearchTextBox.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Developer;
      this.SearchTextBox.DataField = null;
      this.SearchTextBox.DataSource = null;
      this.SearchTextBox.DefinitionFile = "";
      this.SearchTextBox.DictButtonText = "S";
      this.SearchTextBox.DictButtonVisible = true;
      this.SearchTextBox.Dock = System.Windows.Forms.DockStyle.Top;
      this.SearchTextBox.Id = " - [Sente.NetCL.Components.STextBox]";
      this.SearchTextBox.Location = new System.Drawing.Point(0, 0);
      this.SearchTextBox.Name = "SearchTextBox";
      this.SearchTextBox.SCPosition = System.Windows.Forms.DockStyle.None;
      this.SearchTextBox.Size = new System.Drawing.Size(240, 21);
      this.SearchTextBox.TabIndex = 1;
      // 
      // ECONTRACTSDEFGrid
      // 
      this.EMPLOYEESGrid.BindingValue = null;
      this.EMPLOYEESGrid.Caption = null;
      this.EMPLOYEESGrid.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.EMPLOYEESGrid.DataField = null;
      this.EMPLOYEESGrid.DataSource = this.DataEMPLOYEES;
      this.EMPLOYEESGrid.DefinitionFile = "";
      this.EMPLOYEESGrid.DictButtonText = "";
      this.EMPLOYEESGrid.DictButtonVisible = false;
      this.EMPLOYEESGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.EMPLOYEESGrid.Id = "SDataGrid - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGri" +
          "d] - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGrid]";
      this.EMPLOYEESGrid.Location = new System.Drawing.Point(0, 21);
      this.EMPLOYEESGrid.ModifiedCols = null;
      this.EMPLOYEESGrid.Name = "EMPLOYEESGrid";
      this.EMPLOYEESGrid.SCPosition = System.Windows.Forms.DockStyle.None;
      this.EMPLOYEESGrid.SearchRapid = false;
      this.EMPLOYEESGrid.SearchTextBox = this.SearchTextBox;
      this.EMPLOYEESGrid.Size = new System.Drawing.Size(240, 219);
      this.EMPLOYEESGrid.TabIndex = 0;
      // 
      // DictECONTRACTSDEFForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "Wybierz pracownika do operacji";
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Name = "DictEMPLOYEES";
      this.Text = "DictEMPLOYEES";
      this.sFormPanel.ResumeLayout(false);
      this.sPanel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion
    private Sente.NetCL.Database.SBindingSource DataEMPLOYEES;
    private Sente.NetCL.Components.STextBox SearchTextBox;
    private Sente.NetCL.Components.SPanel sPanel1;
    private Sente.NetCL.Components.SButton sButton2;
    private Sente.NetCL.Components.SButton sButton1;
    private Sente.NetCL.Components.SDataGrid EMPLOYEESGrid;

    

  }
}