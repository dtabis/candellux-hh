using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL;
using Sente.NetCL.Database;

namespace Sente.esystemcf.formsProd
{
  public partial class EditPROPERSRAPS : SForm
  {
    public EditPROPERSRAPS()
    {
      InitializeComponent();
    }
    protected override void SetBaseComponents()
    {
      base._sFormComponents = this.components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      this.components = container;
    }

    SDataView _dv;
    private bool EditPROPERSRAPS_FormShow(object sender)
    {
      string sprschedoper = app.Values["PRSCHEDOPER"].AsString;
      _dv = DataPROPERSRAPS.DataView;
      if (_dv.EditState == SViewEditState.Insert)
      {
        //obsluga blankowania
        _dv["PRSCHEDOPER"] = new Variant(sprschedoper);
        SDataView dv = DB.OpenTable("", "PRSHOPERSECONTRDEFS", "", sFillMode.Default, "PRSHOPERSECONTRDEFS", "PRSHOPER;ECONTRACTSDEF");
        if (dv.FindRecord("PRSHOPER", DB.CurrTable("PRSCHEDOPERS")["SHOPER"].AsString))
        {
          _dv["ECONTRACTSDEF"] = dv["ECONTRACTSDEF"];
        }
        _dv["AMOUNT"] = new Variant(AmountDefault(), Sente.NetCL.ValueType.Numeric);
        _dv["MAKETIME"] = new Variant(DateTime.Now, Sente.NetCL.ValueType.DateTime);
      }
      if (_dv.EditState == SViewEditState.Insert || _dv.EditState == SViewEditState.Edit)
      {
        int IlrekEcon = 0;
        string SQL = "select count(*) from PRSHOPERSECONTRDEFS PS where PS.PRSHOPER =0" + DB.CurrTable("PRSCHEDOPERS")["SHOPER"].AsString;
        SDataView dv = DB.OpenTable("", "", SQL, sFillMode.Default, "", "");
        if (dv.FirstRecord())
        {
          IlrekEcon = dv["COUNT"].AsInteger;
        }
        DB.CloseTable(dv.TableName);
        sTextBox4.Enabled = IlrekEcon != 1;
        sTextBox4.SDictParams.DictFilter = "ECONTRACTSDEF.REF in (select PS.ECONTRACTSDEF from PRSHOPERSECONTRDEFS PS where PS.PRSHOPER = (select po.shoper from PRSCHEDOPERS PO where PO.ref = 0" + sprschedoper + "))";
      }
      return true;
    }

    private Decimal AmountDefault()
    {
      string amounts;
      Decimal amount = 0;
      ValuesDictionary vals = new ValuesDictionary();
      vals.SetValue("PRSCHEDOPER", Sente.NetCL.ValueType.Integer, ValueRole.InputValue, _dv["PRSCHEDOPER"].AsInteger);
      vals.SetValue("ECONTRACTSDEF", Sente.NetCL.ValueType.Integer, ValueRole.InputValue, _dv["ECONTRACTSDEF"].AsInteger);
      vals.SetValue("ECONTRACTSPOSDEF", Sente.NetCL.ValueType.Integer, ValueRole.InputValue, _dv["ECONTRACTSPOSDEF"].AsInteger);
      vals.SetValue("AMOUNT", Sente.NetCL.ValueType.Numeric, ValueRole.OutputValue, "0");
      if (DB.GetDatabase("").RunProcedure("PROPERSRAPS_AMOUNTDEFAULT", vals))
      {
        amounts = vals["AMOUNT"].AsString;
        try
        {
          amount = vals["AMOUNT"].AsNumeric;
        }
        catch (Exception e) { ;}
      }
      return amount;

    }

  }
}