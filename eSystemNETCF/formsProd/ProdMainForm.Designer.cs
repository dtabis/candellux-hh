namespace Sente.esystemcf.formsProd
{
  partial class ProdMainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.sButton1 = new Sente.NetCL.Components.SButton(this.components);
      this.sButton2 = new Sente.NetCL.Components.SButton(this.components);
      this.sFormPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.sButton2);
      this.sFormPanel.Controls.Add(this.sButton1);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 244);
      // 
      // sButton1
      // 
      this.sButton1.Location = new System.Drawing.Point(39, 6);
      this.sButton1.Name = "sButton1";
      this.sButton1.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.sButton1.Size = new System.Drawing.Size(150, 48);
      this.sButton1.TabIndex = 0;
      this.sButton1.Text = "Raport produkcji";
      this.sButton1.Click += new System.EventHandler(this.sButton1_Click);
      // 
      // sButton2
      // 
      this.sButton2.Location = new System.Drawing.Point(39, 60);
      this.sButton2.Name = "sButton2";
      this.sButton2.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.sButton2.Size = new System.Drawing.Size(150, 48);
      this.sButton2.TabIndex = 1;
      this.sButton2.Text = "Zako�cz";
      this.sButton2.Click += new System.EventHandler(this.sButton2_Click);
      // 
      // ProdMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "SENTE Podukcja";
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.MinimizeBox = false;
      this.Name = "ProdMainForm";
      this.ShortCaption = "";
      this.Text = "SENTE Produkcja";
      this.sFormPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Sente.NetCL.Components.SButton sButton1;
    private Sente.NetCL.Components.SButton sButton2;


  }
}