using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;

namespace Sente.esystemcf.formsProd
{
  public partial class BrowsePRSCHEDOPERSForm : SForm
  {
    public BrowsePRSCHEDOPERSForm()
    {
      InitializeComponent();
    }
    protected override void SetBaseComponents()
    {
      base._sFormComponents = components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      components = container;
    }

    private void sButton1_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
    }

    private void sButton2_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Cancel;
    }
  }
}