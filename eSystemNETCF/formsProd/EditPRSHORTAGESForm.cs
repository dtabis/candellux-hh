using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sente.NetCL.Gui;
using Sente.NetCL.Gui.Enums;
using Sente.NetCL;
using Sente.NetCL.Database;

namespace Sente.esystemcf.formsProd
{
  public partial class EditPRSHORTAGESForm : SForm
  {
    public EditPRSHORTAGESForm()
    {
      InitializeComponent();
    }
    protected override void SetBaseComponents()
    {
      base._sFormComponents = this.components;
    }
    protected override void SetComponentsFromBase(IContainer container)
    {
      this.components = container;
    }

    string _prdepart;
    int prschedoper;
    int prnagzam;
    SDataView _dv;

    private bool EditPRSHORTAGESForm_FormShow(object sender)
    {
      prschedoper = app.Values["PRSCHEDOPER"].AsInteger;
      prschedoper = 0;
      prnagzam = 0;
      _dv = DataPRSHORTAGES.DataView;
      if (DataPRSHORTAGES.DataView.EditState == SViewEditState.Insert)
      {
        string prschedopers = app.Values["PRSCHEDOPER"].AsString;
        string prnagzams = app.Values["PRNAGZAM"].AsString;
        _dv["PRSCHEDOPER"] = app.Values["PRSCHEDOPER"];
        _dv["PRSCHEDOPERDECL"] = _dv["PRSCHEDOPER"];
        _prdepart = app.Values["CURRENTDEPART"].AsString;
        _dv["PRDEPART"] = app.Values["CURRENTDEPART"];
        SDataView dv_prdeparts = DB.OpenTable("", "PRDEPARTS", "", sFillMode.Default, "PRDEPARTS", "SYMBOL;PRSHORTAGESTYPEDEFAULT");
        if(dv_prdeparts.FindRecord("SYMBOL",app.Values["CURRENTDEPART"].AsString) 
           && dv_prdeparts["PRSHORTAGESTYPEDEFAULT"].AsString != "")
        {
          _dv["PRSHORTAGESTYPE"] = dv_prdeparts["PRSHORTAGESTYPEDEFAULT"];
          _prdepart = dv_prdeparts["SYMBOL"].AsString;
        }
        try
        {
          if(prschedopers != "") prschedoper = Int32.Parse(prschedopers);
          else if(prnagzams != "") prnagzam = Int32.Parse(prnagzams);
        }catch(Exception e){;}
        SDataView dv_opers = DB.OpenTable("","PRSCHEDOPERS","", sFillMode.Default, "PRSCHEDOPERS","AMOUNTIN;AMOUNTSHORTAGES;AMOUNTRESULT;AMOUNTIN");
        dv_opers.SuspendFill = true;
        dv_opers.Filter = "";
        if(dv_opers.FindRecord("REF",prschedoper.ToString()))
        {
          if(dv_opers["AMOUNTIN"].AsNumeric - dv_opers["AMOUNTRESULT"].AsNumeric - dv_opers["AMOUNTSHORTAGES"].AsNumeric > 0)
            DataPRSHORTAGES.DataView["AMOUNT"] = new Variant(dv_opers["AMOUNTIN"].AsNumeric - dv_opers["AMOUNTRESULT"].AsNumeric - dv_opers["AMOUNTSHORTAGES"].AsNumeric, Sente.NetCL.ValueType.Numeric);
        }
//podpowiadanie ktmu brakow
        if(prschedoper > 0 || prnagzam > 0)
        {
          int Ilrek = 0;
          string SQL;
          if(prschedoper > 0)
            SQL = "select count(*) from PRSCHEDGUIDESPOS where PRSCHEDGUIDE = 0" + DB.CurrTable("PRSCHEDOPERS")["GUIDE"].AsString;
          else
            SQL = "select count(*) from POZZAM where ZAMOWIENIE =0"+prnagzam.ToString()+" and OUT = 0";
          SDataView dv_sql = DB.OpenTable("","",SQL,sFillMode.Full,"","");
          if(dv_sql.FirstRecord()) 
          {
            try{
              Ilrek = dv_sql["COUNT"].AsInteger;
            }catch(Exception e){Ilrek = 0;};
          }
          DB.CloseTable(dv_sql.TableName);
          if(Ilrek == 1)
          {
            SDataView dv_gpos = DB.OpenTable("", "PRSCHEDGUIDESPOS", "", sFillMode.Default, "PRSCHEDGUIDESPOS", "KTM;PRSCHEDGUIDE");
            SDataView dv_pozzam = DB.OpenTable("", "POZZAM", "", sFillMode.Default, "POZZAM", "ZAMOWIENIE;OUT;KTM");
            if(prschedoper > 0 && dv_gpos.FindRecord("PRSCHEDGUIDE",DB.CurrTable("PRSCHEDOPERS")["GUIDE"].AsString))
            {
              _dv["KTM"] = dv_gpos["KTM"];
              _dv["PRSCHEDGUIDESPOS"] = dv_gpos["REF"];
            } else if(prnagzam > 0 && dv_pozzam.FindRecord("ZAMOWIENIE;OUT",prnagzam.ToString()+";0"))
            {
              _dv["KTM"] = dv_pozzam["KTM"];
              _dv["PRPOZZAM"] = dv_pozzam["REF"];
            }
            sTextBox2.Enabled = false;
          }else
            sTextBox2.Enabled = true;
        }
      }
      else
      {
        _prdepart = DataPRSHORTAGES.DataView["PRDEPART"].AsString;
      }
      if (DataPRSHORTAGES.DataView.EditState == SViewEditState.Insert || DataPRSHORTAGES.DataView.EditState == SViewEditState.Edit)
      {
        sTextBox1.SDictParams.DictFilter = "[SYMBOL] in (select PSD.PRSHORTAGETYPE from PRSHORTAGESPRDEPARTS PSD where PSD.PRDEPART = '" + _prdepart + "')";
        sTextBox5.SDictParams.DictFilter = "[GUIDE] = " + DB.CurrTable("PRSCHEDOPERS")["GUIDE"].AsString;
        sTextBox5.SDictParams.DictFormName = "BrowsePRSCHEDOPERS";
        sTextBox2.SDictParams.DictFilter = "[PRSCHEDGUIDE] = " + DB.CurrTable("PRSCHEDOPERS")["GUIDE"].AsString;
        sTextBox2.SDictParams.DictField = "KTM^NAZWA";
        sTextBox2.SDictParams.DictFormName = "DictPRSCHEDGUIDESPOS";
        sTextBox2.SDictParams.DictKeyField = "KTM";
        sTextBox2.SDictParams.DictTable = "PRSCHEDGUIDESPOS";
      }
      if (_dv.Current.IsEdit)
      {
        if (sTextBox2.Enabled)
          sTextBox2.Focus();//ustawienei sie na KTM
        else
          sTextBox3.Focus();//ustawienie si� na ilo�ci
      }
      return true;
    }

    private void sTextBox2_AfterEdit(object sender, CancelEventArgs e)
    {
      if (_dv.Current.IsEdit)
      {
        SDataView dv = sTextBox2.SDictParams.DictDataView;
        if (dv != null && !String.IsNullOrEmpty(sTextBox2.Value.AsString))
        {
          dv.SuspendFill = true;
          dv.Filter = sTextBox2.SDictParams.DictFilter;
          if (dv.FindRecord("KTM^NAZWA", sTextBox2.Value.AsString))
          {
            _dv["PRSCHEDGUIDESPOS"] = dv["REF"];
          }
          else
          {
            GUI.ShowMessage("Wybrano towar nie nale��cy do surowc�w przewodnika", "Raport brak�w", SMessageType.Stop);
            e.Cancel = true;
          }
        }
      }
    }

  }
}