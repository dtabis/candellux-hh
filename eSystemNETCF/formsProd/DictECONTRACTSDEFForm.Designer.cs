namespace Sente.esystemcf.formsProd
{
  partial class DictECONTRACTSDEFForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.DataECONTRACTSDEF = new Sente.NetCL.Database.SBindingSource(this.components);
      this.sPanel1 = new Sente.NetCL.Components.SPanel();
      this.sButton2 = new Sente.NetCL.Components.SButton(this.components);
      this.sButton1 = new Sente.NetCL.Components.SButton(this.components);
      this.SearchTextBox = new Sente.NetCL.Components.STextBox(this.components);
      this.ECONTRACTSDEFGrid = new Sente.NetCL.Components.SDataGrid(this.components);
      this.sFormPanel.SuspendLayout();
      this.sPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.ECONTRACTSDEFGrid);
      this.sFormPanel.Controls.Add(this.SearchTextBox);
      this.sFormPanel.Controls.Add(this.sPanel1);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 270);
      // 
      // DataECONTRACTSDEF
      // 
      this.DataECONTRACTSDEF.Active = false;
      this.DataECONTRACTSDEF.AutoActiveEnabled = true;
      this.DataECONTRACTSDEF.Database = "esystem";
      this.DataECONTRACTSDEF.ExpectedFields = "DESCRIPTION;NAME;DOCGROUP;POINTSPERUNIT";
      this.DataECONTRACTSDEF.Id = "ECONTRACTSDEF - [Sente.NetCL.Database.SBindingSource]";
      this.DataECONTRACTSDEF.Initialized = false;
      this.DataECONTRACTSDEF.LocalDataTable = false;
      this.DataECONTRACTSDEF.QuerySql = "";
      this.DataECONTRACTSDEF.TableName = "ECONTRACTSDEF";
      // 
      // sPanel1
      // 
      this.sPanel1.Controls.Add(this.sButton2);
      this.sPanel1.Controls.Add(this.sButton1);
      this.sPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.sPanel1.Location = new System.Drawing.Point(0, 240);
      this.sPanel1.Name = "sPanel1";
      this.sPanel1.Size = new System.Drawing.Size(240, 30);
      // 
      // sButton2
      // 
      this.sButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.sButton2.Location = new System.Drawing.Point(159, 2);
      this.sButton2.Name = "sButton2";
      this.sButton2.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.sButton2.Size = new System.Drawing.Size(75, 25);
      this.sButton2.TabIndex = 3;
      this.sButton2.Text = "Anuluj";
      // 
      // sButton1
      // 
      this.sButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.sButton1.Location = new System.Drawing.Point(78, 2);
      this.sButton1.Name = "sButton1";
      this.sButton1.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.sButton1.Size = new System.Drawing.Size(75, 25);
      this.sButton1.TabIndex = 2;
      this.sButton1.Text = "Zatwierd�";
      // 
      // SearchTextBox
      // 
      this.SearchTextBox.BindingValue = null;
      this.SearchTextBox.Caption = null;
      this.SearchTextBox.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Developer;
      this.SearchTextBox.DataField = null;
      this.SearchTextBox.DataSource = null;
      this.SearchTextBox.DefinitionFile = "";
      this.SearchTextBox.DictButtonText = "S";
      this.SearchTextBox.DictButtonVisible = true;
      this.SearchTextBox.Dock = System.Windows.Forms.DockStyle.Top;
      this.SearchTextBox.Id = " - [Sente.NetCL.Components.STextBox]";
      this.SearchTextBox.Location = new System.Drawing.Point(0, 0);
      this.SearchTextBox.Name = "SearchTextBox";
      this.SearchTextBox.SCPosition = System.Windows.Forms.DockStyle.None;
      this.SearchTextBox.Size = new System.Drawing.Size(240, 21);
      this.SearchTextBox.TabIndex = 1;
      // 
      // ECONTRACTSDEFGrid
      // 
      this.ECONTRACTSDEFGrid.BindingValue = null;
      this.ECONTRACTSDEFGrid.Caption = null;
      this.ECONTRACTSDEFGrid.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.ECONTRACTSDEFGrid.DataField = null;
      this.ECONTRACTSDEFGrid.DataSource = this.DataECONTRACTSDEF;
      this.ECONTRACTSDEFGrid.DefinitionFile = "";
      this.ECONTRACTSDEFGrid.DictButtonText = "";
      this.ECONTRACTSDEFGrid.DictButtonVisible = false;
      this.ECONTRACTSDEFGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ECONTRACTSDEFGrid.Id = "SDataGrid - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGri" +
          "d] - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGrid]";
      this.ECONTRACTSDEFGrid.Location = new System.Drawing.Point(0, 21);
      this.ECONTRACTSDEFGrid.ModifiedCols = null;
      this.ECONTRACTSDEFGrid.Name = "ECONTRACTSDEFGrid";
      this.ECONTRACTSDEFGrid.SCPosition = System.Windows.Forms.DockStyle.None;
      this.ECONTRACTSDEFGrid.SearchRapid = false;
      this.ECONTRACTSDEFGrid.SearchTextBox = this.SearchTextBox;
      this.ECONTRACTSDEFGrid.Size = new System.Drawing.Size(240, 219);
      this.ECONTRACTSDEFGrid.TabIndex = 0;
      // 
      // DictECONTRACTSDEFForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "Wybierz operacj� RCP";
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Name = "DictECONTRACTSDEFForm";
      this.Text = "DictECONTRACTSDEFForm";
      this.sFormPanel.ResumeLayout(false);
      this.sPanel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion
    private Sente.NetCL.Database.SBindingSource DataECONTRACTSDEF;
    private Sente.NetCL.Components.STextBox SearchTextBox;
    private Sente.NetCL.Components.SPanel sPanel1;
    private Sente.NetCL.Components.SButton sButton2;
    private Sente.NetCL.Components.SButton sButton1;
    private Sente.NetCL.Components.SDataGrid ECONTRACTSDEFGrid;

  }
}