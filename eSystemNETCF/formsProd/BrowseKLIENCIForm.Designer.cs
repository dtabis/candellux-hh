namespace Sente.esystemcf.formsProd
{
  partial class BrowseKLIENCIForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.DataKLIENCI = new Sente.NetCL.Database.SBindingSource(this.components);
      this.ButtonPanel = new Sente.NetCL.Components.SPanel();
      this.sButton2 = new Sente.NetCL.Components.SButton(this.components);
      this.sButton1 = new Sente.NetCL.Components.SButton(this.components);
      this.KLIENCIDataGrid = new Sente.NetCL.Components.SDataGrid(this.components);
      this.sTextBox1 = new Sente.NetCL.Components.STextBox(this.components);
      this.sFormPanel.SuspendLayout();
      this.ButtonPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.sTextBox1);
      this.sFormPanel.Controls.Add(this.KLIENCIDataGrid);
      this.sFormPanel.Controls.Add(this.ButtonPanel);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 244);
      // 
      // DataKLIENCI
      // 
      this.DataKLIENCI.Active = false;
      this.DataKLIENCI.AutoActiveEnabled = true;
      this.DataKLIENCI.Database = "";
      this.DataKLIENCI.ExpectedFields = "NAZWA;FSKROT";
      this.DataKLIENCI.Id = "KLIENCI - [Sente.NetCL.Database.SBindingSource]";
      this.DataKLIENCI.Initialized = false;
      this.DataKLIENCI.LocalDataTable = false;
      this.DataKLIENCI.QuerySQL = "";
      this.DataKLIENCI.TableName = "KLIENCI";
      // 
      // ButtonPanel
      // 
      this.ButtonPanel.Controls.Add(this.sButton2);
      this.ButtonPanel.Controls.Add(this.sButton1);
      this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.ButtonPanel.Location = new System.Drawing.Point(0, 214);
      this.ButtonPanel.Name = "ButtonPanel";
      this.ButtonPanel.Size = new System.Drawing.Size(240, 30);
      // 
      // sButton2
      // 
      this.sButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.sButton2.Location = new System.Drawing.Point(166, 3);
      this.sButton2.Name = "sButton2";
      this.sButton2.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.sButton2.Size = new System.Drawing.Size(72, 25);
      this.sButton2.TabIndex = 1;
      this.sButton2.Text = "Anuluj";
      // 
      // sButton1
      // 
      this.sButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.sButton1.Location = new System.Drawing.Point(88, 3);
      this.sButton1.Name = "sButton1";
      this.sButton1.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.sButton1.Size = new System.Drawing.Size(72, 25);
      this.sButton1.TabIndex = 0;
      this.sButton1.Text = "Zatwierd�";
      // 
      // sTextBox1
      // 
      this.sTextBox1.BindingValue = null;
      this.sTextBox1.Caption = "Szukaj";
      this.sTextBox1.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox1.DataField = null;
      this.sTextBox1.DataSource = null;
      this.sTextBox1.DefinitionFile = "";
      this.sTextBox1.DictButtonText = "F3";
      this.sTextBox1.DictButtonVisible = false;
      this.sTextBox1.Dock = System.Windows.Forms.DockStyle.Top;
      this.sTextBox1.Id = "STextBox - [Sente.NetCL.Components.STextBox]";
      this.sTextBox1.Location = new System.Drawing.Point(0, 0);
      this.sTextBox1.Name = "sTextBox1";
      this.sTextBox1.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox1.Size = new System.Drawing.Size(240, 21);
      this.sTextBox1.TabIndex = 3;
      // 
      // KLIENCIDataGrid
      // 
      this.KLIENCIDataGrid.BindingValue = null;
      this.KLIENCIDataGrid.Caption = null;
      this.KLIENCIDataGrid.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.KLIENCIDataGrid.DataField = null;
      this.KLIENCIDataGrid.DataSource = this.DataKLIENCI;
      this.KLIENCIDataGrid.DefinitionFile = "";
      this.KLIENCIDataGrid.DictButtonText = "";
      this.KLIENCIDataGrid.DictButtonVisible = false;
      this.KLIENCIDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.KLIENCIDataGrid.Id = "SDataGrid - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGri" +
          "d] - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGrid]";
      this.KLIENCIDataGrid.Location = new System.Drawing.Point(0, 0);
      this.KLIENCIDataGrid.ModifiedCols = null;
      this.KLIENCIDataGrid.Name = "KLIENCIDataGrid";
      this.KLIENCIDataGrid.SCPosition = System.Windows.Forms.DockStyle.None;
      this.KLIENCIDataGrid.SearchRapid = true;
      this.KLIENCIDataGrid.SearchTextBox = null;
      this.KLIENCIDataGrid.Size = new System.Drawing.Size(240, 214);
      this.KLIENCIDataGrid.TabIndex = 1;
      // 
      // BrowseKLIENCIForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "Wybierz kleinta";
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Name = "BrowseKLIENCIForm";
      this.Text = "BrowseKLIENCIForm";
      this.sFormPanel.ResumeLayout(false);
      this.ButtonPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Sente.NetCL.Database.SBindingSource DataKLIENCI;
    private Sente.NetCL.Components.SPanel ButtonPanel;
    private Sente.NetCL.Components.SButton sButton2;
    private Sente.NetCL.Components.SButton sButton1;
    private Sente.NetCL.Components.SDataGrid KLIENCIDataGrid;
    private Sente.NetCL.Components.STextBox sTextBox1;
  
  }
}