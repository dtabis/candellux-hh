namespace Sente.esystemcf.formsProd
{
  partial class BrowsePRSCHEDOPERSForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.DataPRSCHEDOPERS = new Sente.NetCL.Database.SBindingSource(this.components);
      this.ButtonPanel = new Sente.NetCL.Components.SPanel();
      this.sButton2 = new Sente.NetCL.Components.SButton(this.components);
      this.sButton1 = new Sente.NetCL.Components.SButton(this.components);
      this.PRSCHEDOPERSDataGrid = new Sente.NetCL.Components.SDataGrid(this.components);
      this.sFormPanel.SuspendLayout();
      this.ButtonPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.PRSCHEDOPERSDataGrid);
      this.sFormPanel.Controls.Add(this.ButtonPanel);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 244);
      // 
      // DataPRSCHEDOPERS
      // 
      this.DataPRSCHEDOPERS.Active = false;
      this.DataPRSCHEDOPERS.AutoActiveEnabled = true;
      this.DataPRSCHEDOPERS.Database = "";
      this.DataPRSCHEDOPERS.ExpectedFields = "";
      this.DataPRSCHEDOPERS.Id = "PRSCHEDOPERS - [Sente.NetCL.Database.SBindingSource]";
      this.DataPRSCHEDOPERS.Initialized = false;
      this.DataPRSCHEDOPERS.LocalDataTable = false;
      this.DataPRSCHEDOPERS.QuerySql = "";
      this.DataPRSCHEDOPERS.TableName = "PRSCHEDOPERS";
      // 
      // ButtonPanel
      // 
      this.ButtonPanel.Controls.Add(this.sButton2);
      this.ButtonPanel.Controls.Add(this.sButton1);
      this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.ButtonPanel.Location = new System.Drawing.Point(0, 214);
      this.ButtonPanel.Name = "ButtonPanel";
      this.ButtonPanel.Size = new System.Drawing.Size(240, 30);
      // 
      // sButton2
      // 
      this.sButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.sButton2.Location = new System.Drawing.Point(166, 3);
      this.sButton2.Name = "sButton2";
      this.sButton2.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.sButton2.Size = new System.Drawing.Size(72, 25);
      this.sButton2.TabIndex = 1;
      this.sButton2.Text = "Anuluj";
      // 
      // sButton1
      // 
      this.sButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.sButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.sButton1.Location = new System.Drawing.Point(88, 3);
      this.sButton1.Name = "sButton1";
      this.sButton1.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.sButton1.Size = new System.Drawing.Size(72, 25);
      this.sButton1.TabIndex = 0;
      this.sButton1.Text = "Zatwierd�";
      // 
      // PRSCHEDOPERSDataGrid
      // 
      this.PRSCHEDOPERSDataGrid.Caption = null;
      this.PRSCHEDOPERSDataGrid.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.PRSCHEDOPERSDataGrid.DataField = null;
      this.PRSCHEDOPERSDataGrid.DataSource = this.DataPRSCHEDOPERS;
      this.PRSCHEDOPERSDataGrid.DefinitionFile = "";
      this.PRSCHEDOPERSDataGrid.DictButtonText = "";
      this.PRSCHEDOPERSDataGrid.DictButtonVisible = false;
      this.PRSCHEDOPERSDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PRSCHEDOPERSDataGrid.Id = "SDataGrid - [Sente.NetCL.Components.SDataGrid] - [Sente.NetCL.Components.SDataGri" +
          "d] - [Sente.NetCL.Components.SDataGrid]";
      this.PRSCHEDOPERSDataGrid.Location = new System.Drawing.Point(0, 0);
      this.PRSCHEDOPERSDataGrid.ModifiedCols = null;
      this.PRSCHEDOPERSDataGrid.Name = "PRSCHEDOPERSDataGrid";
      this.PRSCHEDOPERSDataGrid.SCPosition = System.Windows.Forms.DockStyle.None;
      this.PRSCHEDOPERSDataGrid.SearchRapid = true;
      this.PRSCHEDOPERSDataGrid.SearchTextBox = null;
      this.PRSCHEDOPERSDataGrid.Size = new System.Drawing.Size(240, 214);
      this.PRSCHEDOPERSDataGrid.TabIndex = 1;
      // 
      // BrowsePRSCHEDOPERSForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "Wybierz operacj�";
      this.ClientSize = new System.Drawing.Size(240, 268);
      this.Name = "BrowsePRSCHEDOPERSForm";
      this.sFormPanel.ResumeLayout(false);
      this.ButtonPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Sente.NetCL.Database.SBindingSource DataPRSCHEDOPERS;
    private Sente.NetCL.Components.SPanel ButtonPanel;
    private Sente.NetCL.Components.SButton sButton2;
    private Sente.NetCL.Components.SButton sButton1;
    private Sente.NetCL.Components.SDataGrid PRSCHEDOPERSDataGrid;
  }
}