namespace Sente.esystemcf.formsProd
{
  partial class EditPROPERSRAPS
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.BottomPanel = new Sente.NetCL.Components.SPanel();
      this.CancelButton = new Sente.NetCL.Components.SButton(this.components);
      this.ApplyButton = new Sente.NetCL.Components.SButton(this.components);
      this.sTextBox1 = new Sente.NetCL.Components.STextBox(this.components);
      this.DataPROPERSRAPS = new Sente.NetCL.Database.SBindingSource(this.components);
      this.sTextBox2 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox3 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox4 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox5 = new Sente.NetCL.Components.STextBox(this.components);
      this.sTextBox6 = new Sente.NetCL.Components.STextBox(this.components);
      this.sFormPanel.SuspendLayout();
      this.BottomPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // sFormPanel
      // 
      this.sFormPanel.Controls.Add(this.sTextBox6);
      this.sFormPanel.Controls.Add(this.sTextBox5);
      this.sFormPanel.Controls.Add(this.sTextBox4);
      this.sFormPanel.Controls.Add(this.sTextBox3);
      this.sFormPanel.Controls.Add(this.sTextBox2);
      this.sFormPanel.Controls.Add(this.sTextBox1);
      this.sFormPanel.Controls.Add(this.BottomPanel);
      this.sFormPanel.Location = new System.Drawing.Point(0, 24);
      this.sFormPanel.Size = new System.Drawing.Size(240, 270);
      // 
      // BottomPanel
      // 
      this.BottomPanel.Controls.Add(this.CancelButton);
      this.BottomPanel.Controls.Add(this.ApplyButton);
      this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.BottomPanel.Location = new System.Drawing.Point(0, 240);
      this.BottomPanel.Name = "BottomPanel";
      this.BottomPanel.Size = new System.Drawing.Size(240, 30);
      // 
      // CancelButton
      // 
      this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CancelButton.Location = new System.Drawing.Point(165, 3);
      this.CancelButton.Name = "CancelButton";
      this.CancelButton.Role = Sente.NetCL.Components.ButtonRole.CancelButton;
      this.CancelButton.Size = new System.Drawing.Size(72, 25);
      this.CancelButton.TabIndex = 11;
      this.CancelButton.Text = "&Anuluj";
      // 
      // ApplyButton
      // 
      this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.ApplyButton.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.ApplyButton.Location = new System.Drawing.Point(87, 3);
      this.ApplyButton.Name = "ApplyButton";
      this.ApplyButton.Role = Sente.NetCL.Components.ButtonRole.AcceptButton;
      this.ApplyButton.Size = new System.Drawing.Size(72, 25);
      this.ApplyButton.TabIndex = 10;
      this.ApplyButton.Text = "&Zatwierd�";
      // 
      // sTextBox1
      // 
      this.sTextBox1.BindingValue = null;
      this.sTextBox1.Caption = "Pracownik";
      this.sTextBox1.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox1.DataField = "EMPLOYEE^FILENO";
      this.sTextBox1.DataSource = this.DataPROPERSRAPS;
      this.sTextBox1.DefinitionFile = "";
      this.sTextBox1.DictButtonText = "F3";
      this.sTextBox1.DictButtonVisible = false;
      this.sTextBox1.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] - [Sente" +
          ".NetCL.Components.STextBox]";
      this.sTextBox1.Location = new System.Drawing.Point(7, 6);
      this.sTextBox1.Name = "sTextBox1";
      this.sTextBox1.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox1.Size = new System.Drawing.Size(205, 21);
      this.sTextBox1.TabIndex = 1;
      // 
      // DataPROPERSRAPS
      // 
      this.DataPROPERSRAPS.Active = false;
      this.DataPROPERSRAPS.AutoActiveEnabled = true;
      this.DataPROPERSRAPS.Database = "";
      this.DataPROPERSRAPS.ExpectedFields = "";
      this.DataPROPERSRAPS.Id = "PROPERSRAPS - [Sente.NetCL.Database.SBindingSource]";
      this.DataPROPERSRAPS.Initialized = false;
      this.DataPROPERSRAPS.LocalDataTable = false;
      this.DataPROPERSRAPS.QuerySql = "";
      this.DataPROPERSRAPS.TableName = "PROPERSRAPS";
      // 
      // sTextBox2
      // 
      this.sTextBox2.BindingValue = null;
      this.sTextBox2.Caption = "Nazwisko";
      this.sTextBox2.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox2.DataField = "EMPLOYEE^PERSONNAMES";
      this.sTextBox2.DataSource = this.DataPROPERSRAPS;
      this.sTextBox2.DefinitionFile = "";
      this.sTextBox2.DictButtonText = "F3";
      this.sTextBox2.DictButtonVisible = false;
      this.sTextBox2.Enabled = false;
      this.sTextBox2.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] - [Sente" +
          ".NetCL.Components.STextBox]";
      this.sTextBox2.Location = new System.Drawing.Point(7, 31);
      this.sTextBox2.Name = "sTextBox2";
      this.sTextBox2.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox2.Size = new System.Drawing.Size(205, 21);
      this.sTextBox2.TabIndex = 2;
      // 
      // sTextBox3
      // 
      this.sTextBox3.BindingValue = null;
      this.sTextBox3.Caption = "Ilo��";
      this.sTextBox3.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox3.DataField = "AMOUNT";
      this.sTextBox3.DataSource = this.DataPROPERSRAPS;
      this.sTextBox3.DefinitionFile = "";
      this.sTextBox3.DictButtonText = "F3";
      this.sTextBox3.DictButtonVisible = false;
      this.sTextBox3.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] - [Sente" +
          ".NetCL.Components.STextBox]";
      this.sTextBox3.Location = new System.Drawing.Point(128, 85);
      this.sTextBox3.Name = "sTextBox3";
      this.sTextBox3.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox3.Size = new System.Drawing.Size(109, 21);
      this.sTextBox3.TabIndex = 4;
      // 
      // sTextBox4
      // 
      this.sTextBox4.BindingValue = null;
      this.sTextBox4.Caption = "Operacja RCP";
      this.sTextBox4.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox4.DataField = "ECONTRACTSDEF^NAME";
      this.sTextBox4.DataSource = this.DataPROPERSRAPS;
      this.sTextBox4.DefinitionFile = "";
      this.sTextBox4.DictButtonText = "F3";
      this.sTextBox4.DictButtonVisible = false;
      this.sTextBox4.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] - [Sente" +
          ".NetCL.Components.STextBox]";
      this.sTextBox4.Location = new System.Drawing.Point(7, 58);
      this.sTextBox4.Name = "sTextBox4";
      this.sTextBox4.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox4.Size = new System.Drawing.Size(205, 21);
      this.sTextBox4.TabIndex = 3;
      // 
      // sTextBox5
      // 
      this.sTextBox5.BindingValue = null;
      this.sTextBox5.Caption = "Zaroportowano";
      this.sTextBox5.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox5.DataField = "MAKETIME";
      this.sTextBox5.DataSource = this.DataPROPERSRAPS;
      this.sTextBox5.DefinitionFile = "";
      this.sTextBox5.DictButtonText = "F3";
      this.sTextBox5.DictButtonVisible = false;
      this.sTextBox5.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] - [Sente" +
          ".NetCL.Components.STextBox]";
      this.sTextBox5.Location = new System.Drawing.Point(7, 139);
      this.sTextBox5.Name = "sTextBox5";
      this.sTextBox5.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox5.Size = new System.Drawing.Size(230, 21);
      this.sTextBox5.TabIndex = 6;
      // 
      // sTextBox6
      // 
      this.sTextBox6.BindingValue = null;
      this.sTextBox6.Caption = "Przezbrojenie";
      this.sTextBox6.ComponentCreator = Sente.NetCL.Components.SComponentCreator.Designer;
      this.sTextBox6.DataField = "SETUPTIME";
      this.sTextBox6.DataSource = this.DataPROPERSRAPS;
      this.sTextBox6.DefinitionFile = "";
      this.sTextBox6.DictButtonText = "F3";
      this.sTextBox6.DictButtonVisible = false;
      this.sTextBox6.Id = "STextBox - [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] " +
          "- [Sente.NetCL.Components.STextBox] - [Sente.NetCL.Components.STextBox] - [Sente" +
          ".NetCL.Components.STextBox]";
      this.sTextBox6.Location = new System.Drawing.Point(7, 112);
      this.sTextBox6.Name = "sTextBox6";
      this.sTextBox6.SCPosition = System.Windows.Forms.DockStyle.Left;
      this.sTextBox6.Size = new System.Drawing.Size(230, 21);
      this.sTextBox6.TabIndex = 5;
      // 
      // EditPROPERSRAPS
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
      this.AutoScroll = true;
      this.Caption = "Raportowanie operacji";
      this.ClientSize = new System.Drawing.Size(240, 294);
      this.Name = "EditPROPERSRAPS";
      this.FormShow += new Sente.NetCL.Gui.SForm.OnFormShowEventHandler(this.EditPROPERSRAPS_FormShow);
      this.sFormPanel.ResumeLayout(false);
      this.BottomPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Sente.NetCL.Components.SPanel BottomPanel;
    private Sente.NetCL.Components.SButton CancelButton;
    private Sente.NetCL.Components.SButton ApplyButton;
    private Sente.NetCL.Components.STextBox sTextBox4;
    private Sente.NetCL.Components.STextBox sTextBox3;
    private Sente.NetCL.Components.STextBox sTextBox2;
    private Sente.NetCL.Components.STextBox sTextBox1;
    private Sente.NetCL.Database.SBindingSource DataPROPERSRAPS;
    private Sente.NetCL.Components.STextBox sTextBox6;
    private Sente.NetCL.Components.STextBox sTextBox5;
  }
}